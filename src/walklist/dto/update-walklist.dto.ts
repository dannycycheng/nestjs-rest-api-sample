import { PartialType } from '@nestjs/mapped-types';
import { WalklistDto } from './walklist.dto';

export class UpdateWalklistDto extends PartialType(WalklistDto) {}
