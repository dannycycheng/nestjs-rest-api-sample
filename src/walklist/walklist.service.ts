import { Injectable } from '@nestjs/common';
import { WalklistDto } from './dto/walklist.dto';
import { UpdateWalklistDto } from './dto/update-walklist.dto';

@Injectable()
export class WalklistService {
  create(createWalklistDto: WalklistDto) {
    return 'This action adds a new walklist';
  }

  findAll() {
    return `This action returns all walklist`;
  }

  findOne(id: number) {
    return `This action returns a #${id} walklist`;
  }

  update(id: number, updateWalklistDto: UpdateWalklistDto) {
    return `This action updates a #${id} walklist`;
  }

  remove(id: number) {
    return `This action removes a #${id} walklist`;
  }
}
