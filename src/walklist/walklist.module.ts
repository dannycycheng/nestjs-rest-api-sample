import { Module } from '@nestjs/common';
import { WalklistService } from './walklist.service';
import { WalklistController } from './walklist.controller';

@Module({
  controllers: [WalklistController],
  providers: [WalklistService]
})
export class WalklistModule {}
