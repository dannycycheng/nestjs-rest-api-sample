import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { WalklistDto } from './dto/walklist.dto';
import { UpdateWalklistDto } from './dto/update-walklist.dto';
import { WalklistService } from './walklist.service';

@ApiTags('Walklist')
@Controller('walklist')
export class WalklistController {
  constructor(private readonly walklistService: WalklistService) { }

  @ApiOperation({ summary: 'Create a new walklist.' })
  @ApiCreatedResponse({ description: 'New walklist created.' })
  @Post()
  create(@Body() createWalklistDto: WalklistDto) {
    return this.walklistService.create(createWalklistDto);
  }

  @Get()
  findAll() {
    return this.walklistService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.walklistService.findOne(+id);
  }

  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.walklistService.remove(+id);
  }
}
