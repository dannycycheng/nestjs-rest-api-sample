import { Test, TestingModule } from '@nestjs/testing';
import { WalklistService } from './walklist.service';

describe('WalklistService', () => {
  let service: WalklistService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WalklistService],
    }).compile();

    service = module.get<WalklistService>(WalklistService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
