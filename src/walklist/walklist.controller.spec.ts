import { Test, TestingModule } from '@nestjs/testing';
import { WalklistController } from './walklist.controller';
import { WalklistService } from './walklist.service';

describe('WalklistController', () => {
  let controller: WalklistController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WalklistController],
      providers: [WalklistService],
    }).compile();

    controller = module.get<WalklistController>(WalklistController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
