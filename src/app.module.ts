import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TerritoryModule } from './territory/territory.module';
import { WalklistModule } from './walklist/walklist.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'dev',
      password: '1234',
      database: 'terr_mgmt_db',
      autoLoadEntities: true,
      synchronize: false,
    }),
    TerritoryModule,
    WalklistModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
