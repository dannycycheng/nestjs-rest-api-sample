import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TerritoryEntity } from './entities/territory.entity';
import { TerritoryController } from './territory.controller';
import { TerritoryService } from './territory.service';

@Module({
  imports: [TypeOrmModule.forFeature([TerritoryEntity])],
  controllers: [TerritoryController],
  providers: [TerritoryService]
})
export class TerritoryModule { }