import { PartialType } from '@nestjs/mapped-types';
import { TerritoryDto } from './territory.dto';

export class UpdateTerritoryDto extends PartialType(TerritoryDto) { }
