import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { TerritoryEntity } from "../entities/territory.entity";

export class TerritoryDto {
    @ApiProperty({
        required: false,
        readOnly: true,
        description: 'The ID of the created territory.'
    })
    id: number;

    @ApiProperty({
        description: 'Name of the territory.'
    })
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty({
        description: 'Type of territory.'
    })
    @IsString()
    @IsNotEmpty()
    type: string;

    @ApiProperty({
        required: false,
        description: 'Arbitrary attributes about this territory.'
    })
    attributes: JSON;

    @ApiProperty({
        description: 'Geometry of this territory in GeoJSON.'
    })
    @IsNotEmpty()
    geometry: JSON;

    @ApiProperty({
        required: false,
        readOnly: true,
        description: 'Who created this territory.'
    })
    created_by: string;

    @ApiProperty({
        required: false,
        readOnly: true,
        description: 'When was this territory created.'
    })
    created_time: Date;

    @ApiProperty({
        required: false,
        readOnly: true,
        description: 'Who updated this territory.'
    })
    updated_by: string;

    @ApiProperty({
        required: false,
        readOnly: true,
        description: 'When was this territory updated.'
    })
    updated_time: Date;

    static fromEntity(entity: TerritoryEntity): TerritoryDto {
        let dto = new TerritoryDto();
        dto.id = entity.id;
        dto.name = entity.name;
        dto.type = entity.type;
        dto.attributes = entity.attributes;
        dto.geometry = entity.geometry;
        dto.created_by = entity.created_by;
        dto.created_time = entity.created_time;
        dto.updated_by = entity.updated_by;
        dto.updated_time = entity.updated_time;

        return dto;
    }

    toEntity(): TerritoryEntity {
        let entity = new TerritoryEntity();
        entity.name = this.name;
        entity.type = this.type;
        entity.attributes = this.attributes;
        entity.geometry = this.geometry;
        // TODO: once IAM is integrated replace this with username
        entity.created_by = 'System';
        entity.created_time = new Date();

        return entity;
    }
}
