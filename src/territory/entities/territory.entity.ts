import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('territory')
export class TerritoryEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

    @Column('varchar', { nullable: false })
    type: string;

    @Column('json', { nullable: true })
    attributes: JSON;

    @Column({ type: 'geometry', srid: 4326, nullable: false })
    geometry: JSON;

    @Column('varchar', { nullable: false })
    created_by: string;

    @Column('timestamp with time zone', { nullable: false })
    created_time: Date;

    @Column('varchar', { nullable: true })
    updated_by: string;

    @Column('timestamp with time zone', { nullable: true })
    updated_time: Date;
}
