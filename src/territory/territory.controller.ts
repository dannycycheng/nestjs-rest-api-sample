import { Body, Controller, Delete, Get, Param, Patch, Post, UseInterceptors } from '@nestjs/common';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { NotFoundInterceptor } from 'src/interceptors/not-found.interceptor';
import { TerritoryDto } from './dto/territory.dto';
import { UpdateTerritoryDto } from './dto/update-territory.dto';
import { TerritoryService } from './territory.service';

@ApiTags('Territory')
@Controller('territory')
export class TerritoryController {
  constructor(private readonly territoryService: TerritoryService) { }

  @Post()
  @ApiOperation({ summary: 'Create a new territory.' })
  @ApiCreatedResponse({ description: 'New territory created.' })
  @ApiBadRequestResponse({ description: 'Invalid request.' })
  create(@Body() createTerritoryDto: TerritoryDto): Promise<TerritoryDto> {
    return this.territoryService.create(createTerritoryDto);
  }

  @Get()
  @ApiOkResponse({ description: 'All territories.' })
  findAll(): Promise<TerritoryDto[]> {
    return this.territoryService.findAll();
  }

  @Get(':id')
  @UseInterceptors(new NotFoundInterceptor())
  @ApiOkResponse({ description: 'Territory with the provided ID.' })
  @ApiNotFoundResponse({ description: 'Unable to find territory with the provided ID.' })
  findOne(@Param('id') id: number) {
    return this.territoryService.findById(+id);
  }

  @Patch(':id')
  @UseInterceptors(new NotFoundInterceptor())
  @ApiOkResponse({ description: 'Patched territory with the provided ID.' })
  @ApiNotFoundResponse({ description: 'Unable to patch territory with the provided ID.' })
  update(@Param('id') id: number, @Body() updateTerritoryDto: UpdateTerritoryDto) {
    return this.territoryService.update(+id, updateTerritoryDto);
  }

  @Delete(':id')
  @UseInterceptors(new NotFoundInterceptor())
  @ApiOkResponse({ description: 'Deleted territory with the provided ID.' })
  @ApiNotFoundResponse({ description: 'Unable to delete territory with the provided ID.' })
  remove(@Param('id') id: number) {
    return this.territoryService.removeById(+id);
  }
}
