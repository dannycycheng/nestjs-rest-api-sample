import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { TerritoryDto } from './dto/territory.dto';
import { UpdateTerritoryDto } from './dto/update-territory.dto';
import { TerritoryEntity } from './entities/territory.entity';

@Injectable()
export class TerritoryService {
  constructor(@InjectRepository(TerritoryEntity) private repo: Repository<TerritoryEntity>) { }

  async create(createTerritoryDto: TerritoryDto): Promise<TerritoryDto> {
    let newEntity = await this.repo.save(createTerritoryDto.toEntity());
    return TerritoryDto.fromEntity(newEntity);
  }

  async findAll(): Promise<TerritoryDto[]> {
    let entities: TerritoryEntity[] = await this.repo.find();
    let dtos: TerritoryDto[] = [];

    entities.forEach((entity: TerritoryEntity) => {
      dtos.push(TerritoryDto.fromEntity(entity));
    });

    return dtos;
  }

  async findById(id: number): Promise<TerritoryDto> {
    let entity = await this.repo.findOne(id);
    return entity !== undefined ? TerritoryDto.fromEntity(entity) : undefined;
  }

  update(id: number, updateTerritoryDto: UpdateTerritoryDto) {
    return this.repo.update(id, updateTerritoryDto);
  }

  removeById(id: number): Promise<DeleteResult> {
    return this.repo.delete(id);
  }
}
