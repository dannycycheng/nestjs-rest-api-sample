import { Injectable } from '@nestjs/common';
import { name, version } from '../package.json';

@Injectable()
export class AppService {
  getAppInfo(): string {
    return `${name} v${version}`;
  }
}
