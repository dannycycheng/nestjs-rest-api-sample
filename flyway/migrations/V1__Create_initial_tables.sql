CREATE TABLE IF NOT EXISTS territory(
    id              serial primary key,
    name            varchar not null,
    type            varchar not null,
    attributes      json,
    geometry        geometry not null,
    created_by      varchar not null,
    created_time    timestamp with time zone not null,
    updated_by      varchar,
    updated_time    timestamp with time zone
);

CREATE TABLE IF NOT EXISTS walklist(
    id              serial primary key,
    name            varchar not null,
    type            varchar not null,
    attributes      json,
    geometry        geometry not null,
    created_by      varchar not null,
    created_time    timestamp with time zone not null,
    updated_by      varchar,
    updated_time    timestamp with time zone
);

CREATE TABLE IF NOT EXISTS territory_assigned(
    territory_id    int,
    vendor_id       int
);

CREATE TABLE IF NOT EXISTS walklist_assigned(
    territory_id    int,
    walklist_id     int
)