#!/usr/bin/bash

flyway -configFiles=/home/dcheng/projects/terr-mgmt-svc/flyway/flyway.conf -locations="filesystem:/home/dcheng/projects/terr-mgmt-svc/flyway/migrations" $1
